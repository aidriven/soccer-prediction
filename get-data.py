import shutil

import requests


def get_data(name):
    url = 'http://football-data.co.uk/mmz4281/1718/' + name + '.csv'
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        with open('data/' + league + '.csv', 'wb') as file:
            response.raw.decode_content = True
            shutil.copyfileobj(response.raw, file)


leagues = [
    'E0', 'E1', 'E2', 'E3', 'EC',
    'D1', 'D2'
]
for league in leagues:
    get_data(league)
