import pandas as pd

from lib import parse_date, get_gss, get_agg_points, add_form_df, get_mw, get_form_points, get_3game_ws, get_5game_ws, \
    get_3game_ls, get_5game_ls, only_hw

df = pd.read_csv('data/D1.csv')
df.Date = df.Date.apply(parse_date)

columns = ['Date', 'HomeTeam', 'AwayTeam', 'FTHG', 'FTAG', 'FTR']

df = df[columns]
df = get_gss(df)
df = get_agg_points(df)
df = add_form_df(df)

columns = ['Date', 'HomeTeam', 'AwayTeam', 'FTHG', 'FTAG', 'FTR', 'HTGS', 'ATGS', 'HTGC', 'ATGC', 'HTP', 'ATP', 'HM1',
           'HM2', 'HM3',
           'HM4', 'HM5', 'AM1', 'AM2', 'AM3', 'AM4', 'AM5']

df = df[columns]
df = get_mw(df)

df['HTFormPtsStr'] = df['HM1'] + df['HM2'] + df['HM3'] + df['HM4'] + df['HM5']
df['ATFormPtsStr'] = df['AM1'] + df['AM2'] + df['AM3'] + df['AM4'] + df['AM5']
df['HTFormPts'] = df['HTFormPtsStr'].apply(get_form_points)
df['ATFormPts'] = df['ATFormPtsStr'].apply(get_form_points)

df['HTWinStreak3'] = df['HTFormPtsStr'].apply(get_3game_ws)
df['HTWinStreak5'] = df['HTFormPtsStr'].apply(get_5game_ws)
df['HTLossStreak3'] = df['HTFormPtsStr'].apply(get_3game_ls)
df['HTLossStreak5'] = df['HTFormPtsStr'].apply(get_5game_ls)

df['ATWinStreak3'] = df['ATFormPtsStr'].apply(get_3game_ws)
df['ATWinStreak5'] = df['ATFormPtsStr'].apply(get_5game_ws)
df['ATLossStreak3'] = df['ATFormPtsStr'].apply(get_3game_ls)
df['ATLossStreak5'] = df['ATFormPtsStr'].apply(get_5game_ls)

df['HTGD'] = df['HTGS'] - df['HTGC']
df['ATGD'] = df['ATGS'] - df['ATGC']

# Diff in points
df['DiffPts'] = df['HTP'] - df['ATP']
df['DiffFormPts'] = df['HTFormPts'] - df['ATFormPts']

# Diff in last year positions
# df['DiffLP'] = df['HomeTeamLP'] - df['AwayTeamLP']
df['DiffLP'] = 0

cols = ['HTGD', 'ATGD', 'DiffPts', 'DiffFormPts', 'HTP', 'ATP']
df.MW = df.MW.astype(float)

for col in cols:
    df[col] = df[col] / df.MW

df['FTR'] = df.FTR.apply(only_hw)


df.to_csv("data/final_dataset.csv")